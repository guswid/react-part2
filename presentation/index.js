// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  Deck,
  Fill,
  Fit,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Quote,
  Slide,
  SlideSet,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
  fluxDiagram: require("../assets/flux-diagram.png"),
  reactComponents: require("../assets/react-components.png"),
  kat: require("../assets/kat.png"),
  logo: require("../assets/formidable-logo.svg"),
  markdown: require("../assets/markdown.png")
};

preloader(images);

const theme = createTheme({
  primary: "white",
  secondary: "#1F2022",
  tertiary: "#03A9FC",
  quartenary: "#CECECE"
}, {
  primary: "Montserrat",
  secondary: "Helvetica"
});

export default class Presentation extends React.Component {
  renderTitle = () => {
    return (
      <Slide transition={["zoom"]} bgColor="primary">
        <Heading size={1} fit caps lineHeight={1} textColor="secondary">
          React Part 2
        </Heading>
        <Text margin="10px 0 0" textColor="tertiary" size={1} fill bold>
          Redux
        </Text>
      </Slide>
    );
  }

  renderFlux = () => {
    // TODO: Make the react components image hidden at first then shown upon clicked
    return (
      <SlideSet>
        <Slide transition={["fade"]} bgColor="tertiary" textColor="secondary">
          <Heading size={1} textColor="primary" caps>Flux</Heading>
          <List>
            <Appear><ListItem textSize="2.0em" bold>Dispatcher</ListItem></Appear>
            <Appear><ListItem textSize="2.0em" bold>Stores</ListItem></Appear>
            <Appear><ListItem textSize="2.0em" bold>Views</ListItem></Appear>
          </List>
        </Slide>
        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={1} textColor="primary" caps>Flux</Heading>
          <Image src={images.fluxDiagram.replace("/", "")} margin="50px auto 40px" height="293px"/>
        </Slide>
        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={1} textColor="primary" caps>Flux</Heading>
          <Text margin="10px 0 0" textColor="secondary" size={1} fill caps>
            Why
          </Text>
          <Image
            src={images.reactComponents.replace("/", "")}
            margin="50px auto 40px"
            height="293px"
          />
        </Slide>
      </SlideSet>
    );
  }

  renderRedux = () => {
    // Comparison with plain flux (advantage)
    // Concept
    // Principles
    // Examples
  }

  renderReferences = () => {
    return (
      <Slide>
        <Heading size={1} textColor="secondary">References:</Heading>
        <List>
          <ListItem textSize="1.0em">
            <Link href="http://facebook.github.io/flux/docs/in-depth-overview.html">
              Flux schema (http://facebook.github.io/flux/docs/in-depth-overview.html)
            </Link>
          </ListItem>
        </List>
      </Slide>
    );
  }

  render() {
    return (
      <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>
        {this.renderTitle()}
        {this.renderFlux()}
        {this.renderReferences()}
        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={6} textColor="primary" caps>Typography</Heading>
          <Heading size={1} textColor="secondary">Heading 1</Heading>
          <Heading size={2} textColor="secondary">Heading 2</Heading>
          <Heading size={3} textColor="secondary">Heading 3</Heading>
          <Heading size={4} textColor="secondary">Heading 4</Heading>
          <Heading size={5} textColor="secondary">Heading 5</Heading>
          <Text size={6} textColor="secondary">Standard text</Text>
        </Slide>
        <Slide transition={["fade"]} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="secondary" caps>Standard List</Heading>
          <List>
            <ListItem>Item 1</ListItem>
            <ListItem>Item 2</ListItem>
            <ListItem>Item 3</ListItem>
            <ListItem>Item 4</ListItem>
          </List>
        </Slide>
        <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Quote>Example Quote</Quote>
            <Cite>Author</Cite>
          </BlockQuote>
        </Slide>
      </Deck>
    );
  }
}
